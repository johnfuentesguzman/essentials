# README #

This prpject is a backbone in terms of coding and how node js works

### What will I learn? ###

* Creating node server (http module, NOT HTTPS)
* Node lifecycle & events loop
* Understanding http request
* Sending respondes  (including body)
* Routing
* Blocking and non-blocking code

### How to init a project? ###
* npm init: to create a new project


### Restarting the Debugger Automatically After Editing our App ###

* 1. npm i  -g nodemon: code changes Listener 
* 2. in vsc: run/ add configuation / select nodejs as option
* 3. in launch.json file add the below listed properties: 
*  "restart": true,
*  "runtimeExecutable": "nodemon",
*  "console": "integratedTerminal"

example: 
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        
        {
            "type": "node",
            "request": "launch",
            "name": "Launch Program",
            "skipFiles": [
                "<node_internals>/**"
            ],
            "program": "${workspaceFolder}/app.js",
            "restart": true,
            "runtimeExecutable": "nodemon",
            "console": "integratedTerminal"
        }
    ]
}


